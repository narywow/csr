.PHONY: all clean

FLAGS?=-fopenmp
BUILD_DIR?=build
LIB_DIR?=$(BUILD_DIR)/lib

all: clean test

test:	libCSR test.o
	gcc $(BUILD_DIR)/test.o -o $(BUILD_DIR)/test -L$(LIB_DIR) -lCSR $(FLAGS)
	chmod u+x $(BUILD_DIR)/test

test.o:
	gcc -c ./src/test.c -o $(BUILD_DIR)/test.o $(FLAGS)

libCSR: libCSR.o
	ar rc $(LIB_DIR)/libCSR.a $(LIB_DIR)/libCSR.o

libCSR.o:
	if [ ! -d $(BUILD_DIR) ]; then mkdir $(BUILD_DIR); fi
	if [ ! -d $(LIB_DIR) ]; then mkdir $(LIB_DIR); fi
	gcc -c ./src/lib/libCSR.c -o $(LIB_DIR)/libCSR.o $(FLAGS)

clean:
	rm -rf $(BUILD_DIR)
