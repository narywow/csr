#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "libCSR.h"

csr init_csr(const int n_row, const int n_col, const int allocation_chunk)
{
	csr rez;

	rez.n_row = n_row;
	rez.n_col = n_col;
	rez.n_nnz = 0;

	rez.row_index = malloc((n_row + 1) * sizeof(int));
	rez.row_index[0] = 0;

	rez.val = malloc(allocation_chunk * sizeof(double));
	rez.col_index = malloc(allocation_chunk * sizeof(int));

	return rez;
}

csr allocate_array_to_csr(const double* array, const int n_row, const int n_col)
{
	//number of nonzero element of a sparce matrix (n,n) of order n
	const int sparce_koef = 3;
	const int allocation_chunk = n_col >= n_row ? sparce_koef * n_col : sparce_koef * n_row;

	//rezult CSR
	csr rez = init_csr(n_row, n_col, allocation_chunk);
	int n_val_size = allocation_chunk;

	for (int i = 0; i < n_row; i++)
	{
		for (int j = 0; j < n_col; j++)
		{
			if (array[i * n_col + j] == 0) continue;

			if (rez.n_nnz > n_val_size)
			{
				n_val_size += allocation_chunk;
				rez.val = realloc(rez.val, n_val_size * sizeof(double));
				rez.col_index = realloc(rez.col_index, n_val_size * sizeof(int));
			}

			rez.n_nnz++;
			rez.val[rez.n_nnz - 1] = array[i * n_col + j];
			rez.col_index[rez.n_nnz - 1] = j;

		}
		rez.row_index[i + 1] = rez.n_nnz;
	}

	rez.val = realloc(rez.val, rez.n_nnz * sizeof(double));
	rez.col_index = realloc(rez.col_index, rez.n_nnz * sizeof(int));

	return rez;
}

csr allocate_matrix_to_csr(const double** matrix, const int n_row, const int n_col)
{
	//number of nonzero element of a sparce matrix (n,n) of order n
	const int sparce_koef = 3;
	const int allocation_chunk = n_col >= n_row ? sparce_koef * n_col : sparce_koef * n_row;

	//rezult CSR
	csr rez = init_csr(n_row, n_col, allocation_chunk);
	int val_size = allocation_chunk;

	for (int i = 0; i < n_row; i++)
	{
		for (int j = 0; j < n_col; j++)
		{
			if (matrix[i][j] == 0) continue;

			if (rez.n_nnz > val_size)
			{
				val_size += allocation_chunk;
				rez.val = realloc(rez.val, val_size * sizeof(double));
				rez.col_index = realloc(rez.col_index, val_size * sizeof(int));
			}

			rez.n_nnz++;
			rez.val[rez.n_nnz - 1] = matrix[i][j];
			rez.col_index[rez.n_nnz - 1] = j;
		}
		rez.row_index[i + 1] = rez.n_nnz;
	}

	rez.val = realloc(rez.val, rez.n_nnz * sizeof(double));
	rez.col_index = realloc(rez.col_index, rez.n_nnz * sizeof(int));

	return rez;
}

double* allocate_mult_csr_vector(const csr csr_matrix, const double* vector)
{
	//rezult vector
	double* rez = calloc(csr_matrix.n_row, sizeof(double));

	for (int i = 0; i < csr_matrix.n_row; i++)
	{
		for (int j = csr_matrix.row_index[i]; j < csr_matrix.row_index[i + 1]; j++)
		{
			rez[i] += csr_matrix.val[j] * vector[csr_matrix.col_index[j]];
		}
	}

	return rez;
}

double** allocate_csr_to_matrix(const csr csr_matrix)
{
	double** rez = malloc(csr_matrix.n_row * sizeof(double*));
	for (int i = 0; i < csr_matrix.n_row; i++)
	{
		rez[i] = calloc(csr_matrix.n_col, sizeof(double));
	}

	for (int i = 0; i < csr_matrix.n_row; i++)
	{
		for (int j = csr_matrix.row_index[i]; j < csr_matrix.row_index[i + 1]; j++)
		{
			rez[i][csr_matrix.col_index[j]] = csr_matrix.val[j];
		}
	}

	return rez;
}

double* allocate_csr_to_array(const csr A)
{
	double* rez = calloc(A.n_row * A.n_col, sizeof(double));

	for (int i = 0; i < A.n_row; i++)
	{
		for (int j = A.row_index[i]; j < A.row_index[i + 1]; j++)
		{
			rez[i * A.n_col + A.col_index[j]] = A.val[j];
		}
	}

	return rez;
}

int comp(const void* x, const void* y)
{
	return *(int*)x - *(int*)y;
}

csr allocate_random_generate_csr(const int n_row, const int n_col, const int n_nnz_in_row)
{
	srand(time(0));

	csr rezult = init_csr(n_row, n_col, n_col * n_nnz_in_row);

	rezult.n_nnz = n_col * n_nnz_in_row;

	for (int i = 0; i < n_row; i++)
	{
		rezult.col_index[i * n_nnz_in_row] = i;

		for (int j = n_col - n_nnz_in_row + 1; j < n_col; j++)
		{
			int elem = rand() % (j + 1);
			bool isContain = false;
			for (int k = 0; k < j - (n_col - n_nnz_in_row); k++)
			{
				if (rezult.col_index[i * n_nnz_in_row + k] == elem)
				{
					isContain = true;
					break;
				}
			}
			rezult.col_index[i * n_nnz_in_row + j - (n_col - n_nnz_in_row)] = isContain ? j : elem;
		}

		qsort(&rezult.col_index[i * n_nnz_in_row], n_nnz_in_row, sizeof(int), comp);

		for (int j = 0; j < n_nnz_in_row; j++)
		{
			rezult.val[i * n_nnz_in_row + j] = (double)rand() / (RAND_MAX / 2) - 1.;
		}

		rezult.row_index[i + 1] = rezult.row_index[i] + n_nnz_in_row;
	}

	return rezult;
}

void free_csr(csr csr_matrix)
{
	free(csr_matrix.col_index);
	free(csr_matrix.row_index);
	free(csr_matrix.val);
}
