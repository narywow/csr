typedef struct
{
	int n_row;	//rows count
	int n_col;	//columns count
	int n_nnz;	//nonzero elements count
	double* val;	//array with nonzero elements
	int* col_index;	//array with column index of nonzero element
	int* row_index;	//array with count nonzero elements from 
			//first row to i'st row. First element of array is 0
}csr;

//initialization csr struct
csr init_csr(const int n_row, const int n_col, const int allocation_chunk);

//convert matrix in one-dimensional array to CSR format
csr allocate_array_to_csr(const double* array, const int n_row, const int n_col);

//convert matrix in two-dimensional array to CSR format
csr allocate_matrix_to_csr(const double** matrix, const int n_row, const int n_col);

//multiplicate CSR format matrix with vector
double* allocate_mult_csr_vector(const csr csr_matrix, const double* vector);

//convert CSR matrix to two-dimensional array
double** allocate_csr_to_matrix(const csr csr_matrix);

//convert CSR matrix to one-dimensional array
double* allocate_csr_to_array(const csr csr_matrix);

//generate random CSR matrix witn n_nnz_in_row nonzero element in row
csr allocate_random_generate_csr(const int n_row, const int n_col, const int n_nnz_in_row);

//free memory from array in csr
void free_csr(csr csr_matrix);
