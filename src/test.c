#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <omp.h>
#include "./lib/libCSR.h"

void test_matrix_to_csr(double** matrix, int n_row, int n_col, csr ref)
{
	csr temp = allocate_matrix_to_csr(matrix, n_row, n_col);
	assert(temp.n_col == ref.n_col);
	assert(temp.n_row == ref.n_row);
	assert(temp.n_nnz == ref.n_nnz);

	for (int i = 0; i < ref.n_nnz; i++)
	{
		assert(fabs(temp.val[i] - ref.val[i]) <= DBL_EPSILON * fabs(ref.val[i]));
		assert(temp.col_index[i] == ref.col_index[i]);
	}

	for (int i = 0; i < ref.n_row + 1; i++)
	{
		assert(temp.row_index[i] == ref.row_index[i]);
	}

	free_csr(temp);

}
void test_array_to_csr(double* array, int n_row, int n_col, csr ref)
{
	csr temp = allocate_array_to_csr(array, n_row, n_col);

	assert(temp.n_col == ref.n_col);
	assert(temp.n_row == ref.n_row);
	assert(temp.n_nnz == ref.n_nnz);

	for (int i = 0; i < ref.n_nnz; i++)
	{
		assert(fabs(temp.val[i] - ref.val[i]) <= DBL_EPSILON * fabs(ref.val[i]));
		assert(temp.col_index[i] == ref.col_index[i]);
	}

	for (int i = 0; i < ref.n_row + 1; i++)
	{
		assert(temp.row_index[i] == ref.row_index[i]);
	}

	free_csr(temp);
}
void test_csr_to_matrix(double** ref, int n_row, int n_col, csr csr_matrix)
{
	double** temp = allocate_csr_to_matrix(csr_matrix);

	for (int i = 0; i < n_row; i++)
	{
		for (int j = 0; j < n_col; j++)
		{
			assert(fabs(ref[i][j] - temp[i][j]) <= DBL_EPSILON * fabs(ref[i][j]));
		}
	}

	for (int i = 0; i < n_row; i++)
	{
		free(temp[i]);
	}
	free(temp);

}
void test_csr_to_array(double* ref, int n_row, int n_col, csr csr_matrix)
{
	double* temp = allocate_csr_to_array(csr_matrix);

	for (int i = 0; i < n_row; i++)
	{
		for (int j = 0; j < n_col; j++)
		{
			assert(fabs(ref[i * n_col + j] - temp[i * n_col + j]) <= DBL_EPSILON * fabs(ref[i * n_col + j]));
		}
	}

	free(temp);

}
void test_mult_csr_vector(csr csr_matrix, double* vector, double* ref)
{
	double* temp = allocate_mult_csr_vector(csr_matrix, vector);

	for (int i = 0; i < csr_matrix.n_row; i++)
	{
		assert(fabs(temp[i] - ref[i]) <= DBL_EPSILON * fabs(temp[i]));
	}

	free(temp);
}
void test_generate_random_csr(int n_row, int n_col, int nnz_in_row)
{
	csr random_csr = allocate_random_generate_csr(n_row, n_col, nnz_in_row);
	double* random_matrix = allocate_csr_to_array(random_csr);

	for (int i = 0; i < n_row; i++)
	{
		int count_nnz_in_row = 0;
		for (int j = 0; j < n_col; j++)
		{
			if (random_matrix[i * n_col + j] != 0)
			{
				count_nnz_in_row++;
			}
		}
		assert(random_matrix[i * n_col + i] != 0);
		assert(count_nnz_in_row == nnz_in_row);
	}

	free(random_matrix);
	free_csr(random_csr);
}

void time_test(int n_row, int n_col, int nnz_in_row)
{
	double time = omp_get_wtime();
	csr test_csr = allocate_random_generate_csr(n_row, n_col, nnz_in_row);
	printf("CSR matrix with %d rows, %d columns and %d nonzero elements in row generated in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	time = omp_get_wtime();
	double* test_array = allocate_csr_to_array(test_csr);
	printf("CSR matrix with %d rows, %d columns and %d nonzero elements in row converted to one-dimensional array in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	time = omp_get_wtime();
	double** test_matrix = allocate_csr_to_matrix(test_csr);
	printf("CSR matrix with %d rows, %d columns and %d nonzero elements in row converted to two-dimensional array in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	free_csr(test_csr);

	time = omp_get_wtime();
	test_csr = allocate_array_to_csr(test_array, n_row, n_col);
	printf("Matrix in one-dimensional array with %d rows, %d columns and %d nonzero elements in row converted to CSR matrix in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	free_csr(test_csr);

	time = omp_get_wtime();
	test_csr = allocate_matrix_to_csr(test_matrix, n_row, n_col);
	printf("Matrix in two-dimensional array with %d rows, %d columns and %d nonzero elements in row converted to CSR matrix in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	double* test_vector = malloc(n_col * sizeof(double));

	time = omp_get_wtime();
	double* test_rezult_vector = allocate_mult_csr_vector(test_csr, test_vector);
	printf("CSR matrix with %d rows, %d columns and %d nonzero elements in row multiplicated with vector in %f seconds\n\n", n_row, n_col, nnz_in_row, omp_get_wtime() - time);

	for (int i = 0; i < n_row; i++)
	{
		free(test_matrix[i]);
	}

	free_csr(test_csr);
	free(test_array);
	free(test_matrix);
	free(test_vector);
	free(test_rezult_vector);

}
int main(void)
{
	int n_row = 3, n_col = 5;

	double test1_val[] = { 0.5, -0.7, 0.25, -0.9, 15.5, 0.66 };
	int test1_col[] = { 0, 4, 0, 2, 1, 2 };
	int test1_row[] = { 0, 2, 4, 6 };

	csr test1_csr =
	{
		.n_row = n_row,
		.n_col = n_col,
		.n_nnz = 6,
		.val = test1_val,
		.col_index = test1_col,
		.row_index = test1_row
	};

	double** test1_martix = malloc(n_row * sizeof(double*));
	for (int i = 0; i < n_row; i++)
	{
		test1_martix[i] = malloc(n_col * sizeof(double));
	}

	test1_martix[0][0] = .5;
	test1_martix[0][1] = 0;
	test1_martix[0][2] = 0;
	test1_martix[0][3] = 0;
	test1_martix[0][4] = -0.7;
	test1_martix[1][0] = 0.25;
	test1_martix[1][1] = 0;
	test1_martix[1][2] = -0.9;
	test1_martix[1][3] = 0;
	test1_martix[1][4] = 0;
	test1_martix[2][0] = 0;
	test1_martix[2][1] = 15.5;
	test1_martix[2][2] = 0.66;
	test1_martix[2][3] = 0;
	test1_martix[2][4] = 0;

	double test1_array[] = { 0.5, 0, 0, 0, -0.7,
				0.25, 0, -0.9, 0, 0,
				0, 15.5, 0.66 ,0 ,0 };

	double test1_vector[] = { 1, 2, 3, 4, 5 };
	double test1_mult_rezult[] = { -3, -2.45, 32.98 };

	test_matrix_to_csr(test1_martix, n_row, n_col, test1_csr);
	test_csr_to_matrix(test1_martix, n_row, n_col, test1_csr);
	test_mult_csr_vector(test1_csr, test1_vector, test1_mult_rezult);
	test_array_to_csr(test1_array, n_row, n_col, test1_csr);
	test_csr_to_array(test1_array, n_row, n_col, test1_csr);
	test_generate_random_csr(1000, 2000, 50);

	printf("\nPassed!\n\n");

	time_test(2000, 3000, 150);

	for (int i = 0; i < n_row; i++)
	{
		free(test1_martix[i]);
	}
	free(test1_martix);




	return 0;
}
